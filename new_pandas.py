import requests
import time
import pandas as pd
import re
from pathlib import Path
from bs4 import BeautifulSoup as bs
from time import gmtime, strftime

class bcolors:
    GREEN = '\033[92m'
    RED = '\033[91m'
    ENDC = '\033[0m'

regex = r"(?<=\>).+?(?=\<)"

urls = ['https://finance.yahoo.com/quote/BOVA11.SA/',
        'https://finance.yahoo.com/quote/ABEV/',
        'https://finance.yahoo.com/quote/MGLU3.SA/',
        'https://finance.yahoo.com/quote/BBDC4.SA/',
        'https://finance.yahoo.com/quote/PETR4.SA/',
        'https://finance.yahoo.com/quote/ITSA4.SA/',
        'https://finance.yahoo.com/quote/BPAC5.SA/',
        'https://finance.yahoo.com/quote/AZUL4.SA/',
        'https://finance.yahoo.com/quote/GOL/']

outfile = 'stocks_out.csv'
tickers = []
currents = []
changes = []
prevs = []
opns = []
bids = []
asks = []
caps = []

#while True:
date = strftime("%Y-%m-%d %H:%M:%S", gmtime())

for url in urls:
    ticker = url.split('/')[-2]
    print(ticker)
    tickers.append(ticker)
    page = requests.get(url)
    soup = bs(page.content, 'html.parser')
    webdf = pd.read_html(page.text)

    for i in soup.find_all('span'):
        #print(i)
        if('Trsdu(0.3s) Fw(b) Fz(36px) Mb(-4px) D(ib)' in str(i)):
            s = re.search(regex, str(i).strip()) #capture between ><
            if(s != None):
                current = s.group(0)
                currents.append(current)
                print('Current price: ' + current)
        elif('Trsdu(0.3s) Fw(500) Pstart(10px) Fz(24px) C($negativeColor)' in str(i)): #NEGATIVE
            s = re.search(regex, str(i).strip())
            if(s != None):
                change = s.group(0)
                changes.append(change)
                print('Price change: ' + bcolors.RED + change + bcolors.ENDC)
        elif('Trsdu(0.3s) Fw(500) Pstart(10px) Fz(24px) C($positiveColor)' in str(i)): #POSITIVE
            s = re.search(regex, str(i).strip())
            if(s != None):
                change = s.group(0)
                changes.append(change)
                print('Price change: ' + bcolors.GREEN + change + bcolors.ENDC)

    prev = webdf[0].iloc[0, 1]
    prevs.append(prev)
    print('Previous close: ' + prev)

    open = webdf[0].iloc[1, 1]
    opns.append(open)
    print('Open: ' + open)

    bid = webdf[0].iloc[2, 1]
    bids.append(bid)
    print('Bid: ' + bid)

    ask = webdf[0].iloc[3, 1]
    asks.append(ask)
    print('Ask: ' + ask)

    cap = webdf[1].iloc[0, 1]
    caps.append(cap)
    print('Market cap: ' + str(cap))

df = pd.DataFrame(
    {
        'Date': date,
        'Ticker': pd.Categorical(tickers),
        'Current price': pd.Categorical(currents),
        'Price change': pd.Categorical(changes),
        'Previous close': pd.Categorical(prevs),
        'Open': pd.Categorical(opns),
        'Bid': pd.Categorical(bids),
        'Ask': pd.Categorical(asks),
        'Market cap': pd.Categorical(caps),
    }
)
print(df)

if(Path(outfile).is_file()):
    df.to_csv(outfile, mode='a', header=False, index=False)
else:
    df.to_csv(outfile, mode='a', header=True, index=False)

#time.sleep(2)
