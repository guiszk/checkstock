import pandas as pd
import yfinance as yf

tickers = ['BOVA11.SA', 'ABEV', 'MGLU3.SA', 'BBDC4.SA', 'PETR4.SA', 'ITSA4.SA', 'BPAC5.SA', 'AZUL4.SA', 'GOL']

for t in tickers:
    current = yf.Ticker(t)
    df = pd.DataFrame(current.history(period='max'))
    outfile = str(t.replace('.', '') + '.csv')
    df.to_csv(outfile, mode='a', header=True, index=True)
